#lang racket

;-----------------------------------------------------------------------
; Contracts and data structures
;-----------------------------------------------------------------------

(define pos/c (vector/c integer? integer?))
(define seacucumber/c (or/c 'south 'east 'all))

(define seacucumbers/c (hash/c seacucumber/c (set/c pos/c)))

(struct seabed (height width seacucumbers)
  #:transparent)
(provide
  (contract-out
    [struct seabed ((height integer?)
                    (width integer?)
                    (seacucumbers seacucumbers/c))]))

(struct step (from to))
(provide
  (contract-out
    [struct step ((from pos/c)
                  (to pos/c))]))

;-----------------------------------------------------------------------
; Seabed functions
;-----------------------------------------------------------------------

(define (maybe-get-step a-seabed pos sc)
  (let ([maybe-next (maybe-get-next-pos a-seabed pos sc)])
    (if maybe-next
      (step pos maybe-next)
      #f)))
(provide
  (contract-out
    [maybe-get-step (-> seabed? pos/c seacucumber/c step?)]))

(define (maybe-get-next-pos a-seabed pos sc)
  (let ([projected-pos (get-projected-pos a-seabed pos sc)])
    (if (set-member?
          (hash-ref (seabed-seacucumbers a-seabed) 'all)
          projected-pos)
      #f
      projected-pos)))
(provide
  (contract-out
    [maybe-get-next-pos
      (-> seabed? pos/c seacucumber/c (or/c pos/c #f))]))

(define (pos-add a-seabed pos1 pos2)
  (match (list pos1 pos2)
    [(list (vector row1 col1) (vector row2 col2))
     (vector
       (modulo (+ row1 row2) (seabed-height a-seabed))
       (modulo (+ col1 col2) (seabed-width a-seabed)))]))
(provide
  (contract-out
    [pos-add (-> seabed? pos/c pos/c pos/c)]))

(define (get-projected-pos a-seabed pos sc)
  (pos-add
    a-seabed
    pos
    (cond
      [(eq? sc 'south) #(1 0)]
      [(eq? sc 'east) #(0 1)])))
(provide
  (contract-out
    [get-projected-pos (-> seabed? pos/c seacucumber/c pos/c)]))

(define (maybe-step-seabed a-seabed)
  (let* ([maybe-after-east (maybe-step-seabed/east a-seabed)]
         [maybe-after-south
           (maybe-step-seabed/south
             (if maybe-after-east
               maybe-after-east
               a-seabed))])
      (if (false? maybe-after-south)
        (if (false? maybe-after-east)
          #f
          maybe-after-east)
        maybe-after-south)))
(provide
  (contract-out
    [maybe-step-seabed (-> seabed? (or/c seabed? #f))]))

(define (maybe-step-seabed/direction a-seabed direction)
  (let* ([posns (hash-ref (seabed-seacucumbers a-seabed) direction)]
         [steps
           (filter-not
             false?
             (set-map
               posns
               (lambda (pos)
                 (maybe-get-step a-seabed pos direction))))])
    (if (null? steps)
      #f
      (struct-copy
        seabed a-seabed
        [seacucumbers
          (update-seacucumbers
            (seabed-seacucumbers a-seabed)
            steps
            direction)]))))
(define (maybe-step-seabed/east a-seabed)
  (maybe-step-seabed/direction a-seabed 'east))
(define (maybe-step-seabed/south a-seabed)
  (maybe-step-seabed/direction a-seabed 'south))
(provide
  (contract-out
    [maybe-step-seabed/direction
      (-> seabed? seacucumber/c (or/c seabed? #f))]))

(define (update-seacucumbers seacucumbers steps direction)
  (let ([posns-all (hash-ref seacucumbers 'all)]
        [posns-direction (hash-ref seacucumbers direction)]
        [posns-from (list->set (map step-from steps))]
        [posns-to (list->set (map step-to steps))])
    (hash-set
      (hash-set
        seacucumbers
        'all
        (set-union (set-subtract posns-all posns-from) posns-to))
      direction
      (set-union (set-subtract posns-direction posns-from) posns-to))))
(provide
  (contract-out
    [update-seacucumbers
      (-> seacucumbers/c (listof step?) seacucumber/c seacucumbers/c)]))

(define (make-steps posns steps)
  (foldl
    (lambda (a-step posns)
      (set-add
        (set-remove posns (step-from a-step))
        (step-to a-step)))
    posns
    steps))
(provide
  (contract-out
    [make-steps (-> (set/c pos/c) (listof step?) (set/c pos/c))]))

(define (get-step-to-static-count a-seabed)
  (define (iter n a-seabed)
    (if (false? a-seabed)
      n
      (iter
        (+ n 1)
        (maybe-step-seabed a-seabed))))
  (iter 0 a-seabed))

;-----------------------------------------------------------------------
; Part one
;-----------------------------------------------------------------------

(define (part-1 a-seabed)
  (get-step-to-static-count a-seabed))
(provide
  (contract-out
    [part-1 (-> seabed? integer?)]))

;-----------------------------------------------------------------------
; Parsing
;-----------------------------------------------------------------------

(define (parse-input filename)
  (parse-lines (file->lines filename)))
(define (parse-lines lines)
  (define (pairs->hash-set pairs)
    (define (iter south east all pairs)
      (if (null? pairs)
        (make-immutable-hash
          (list (cons 'south south)
                (cons 'east east)
                (cons 'all all)))
        (match (first pairs)
          [(cons pos dir)
           (cond
             [(eq? dir 'south)
              (iter (set-add south pos)
                    east
                    (set-add all pos)
                    (rest pairs))]
             [(eq? dir 'east)
              (iter south
                    (set-add east pos)
                    (set-add all pos)
                    (rest pairs))])])))
    (iter (set) (set) (set) pairs))
  (let* ([char-lines (map string->list lines)]
         [height (length char-lines)]
         [width (length (first char-lines))])
    (seabed
      height
      width
      (pairs->hash-set
        (append*
          (for/list ([chars char-lines]
                     [i (in-naturals)])
            (for/list ([char chars]
                       [j (in-naturals)]
                       #:unless (char=? char #\.))
              (cons (vector i j)
                    (cond
                      [(char=? char #\v) 'south]
                      [(char=? char #\>) 'east])))))))))
(provide
  (contract-out
    [parse-input (-> string? seabed?)]
    [parse-lines (-> (listof string?) seabed?)]))

;-----------------------------------------------------------------------
; Main
;-----------------------------------------------------------------------

(module+ main
  (displayln "AOC 2021 Day 25")
  (define input (parse-input "input"))
  (displayln
    (format "Part one: ~s" (part-1 input)))
  (void))
