#lang racket

(require "main.rkt")

(module+ test
  (require rackunit)

  (check-equal?
    (parse-lines
      '(".." ">."))
    (seabed
      2 2
      (make-immutable-hash
        (list (cons 'south (set))
              (cons 'east (set #(1 0)))
              (cons 'all (set #(1 0)))))))

  (define seabed-1
    (parse-lines '(">>.")))

  (check-false
    (maybe-get-next-pos seabed-1 #(0 0) 'east))

  (check-equal?
    (maybe-get-next-pos seabed-1 #(0 1) 'east)
    #(0 2))

  (define seabed-2
    (parse-lines
      '("..."
        "...")))

  (check-equal?
    (get-projected-pos
      seabed-2
      #(0 2)
      'east)
    #(0 0))

  (check-equal?
    (get-projected-pos
      seabed-2
      #(0 2)
      'south)
    #(1 2))

  (define input-test (parse-input "input-test"))

  (check-equal?
    (part-1 input-test)
    58)

  ;(check-equal?
  ;  (part-2 input-test)
  ;  0)

  (void))
