#lang racket/base

(require "main.rkt")

(module+ test

  (require rackunit)

  (define input-test (parse-input "input-test"))

  (check-equal?
    (part-1 input-test)
    45)

  (check-equal?
    (part-2 input-test)
    112)

  (void))
