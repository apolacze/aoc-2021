#lang racket/base

(require "main.rkt")

(module+ test
  (require rackunit)

  (define input-test (parse-input "input-test"))

  (check-equal?
    (part-1 input-test)
    739785)

  (check-equal?
    (part-2 input-test)
    444356092776315)

  (void))
