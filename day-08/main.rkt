#lang racket

;-----------------------------------------------------------------------
; Util
;-----------------------------------------------------------------------

(define (string->set s)
  (list->set (string->list s)))

(define (snoc x y)
  (cons y x))

(define (alphabetize s)
  (list->string
    (sort (string->list s) char<?)))
(provide
  (contract-out
    [alphabetize (-> string? string?)]))

(define (list->integer xs)
  (string->number
    (apply
      string-append
      (map number->string xs))))
(provide
  (contract-out
    (list->integer (-> (listof integer?) integer?))))

;-----------------------------------------------------------------------
; Panel data structures and functions
;-----------------------------------------------------------------------

(define digits
  (make-immutable-hash
    (list
      (snoc 0 (string->set "abcefg"))
      (snoc 1 (string->set "cf"))
      (snoc 2 (string->set "acdeg"))
      (snoc 3 (string->set "acdfg"))
      (snoc 4 (string->set "bcdf"))
      (snoc 5 (string->set "abdfg"))
      (snoc 6 (string->set "abdefg"))
      (snoc 7 (string->set "acf"))
      (snoc 8 (string->set "abcdefg"))
      (snoc 9 (string->set "abcdfg")))))

(define (pairwise/length/sorted s ss)
  (define (pairwise/length s ss)
    (map (lambda (x) (length (set->list x)))
         (pairwise s ss)))
  (define (pairwise s ss)
    (map (lambda (t) (set-intersect s t))
         ss))
  (sort (pairwise/length s ss) <))

(define digits-dict
  (let ([keys (hash-keys digits)])
    (make-immutable-hash
      (for/list
        ([k keys])
        (cons (pairwise/length/sorted k keys)
              (hash-ref digits k))))))

(define (make-dict signals)
  (let* ([sets (map (lambda (s) (list->set (string->list s))) signals)]
         [pls (map (lambda (s) (pairwise/length/sorted s sets)) sets)])
    (make-immutable-hash
      (for/list
        ([s signals]
         [k pls])
        (cons s (hash-ref digits-dict k))))))

(define (get-panel-output panel)
  (let ([dd (make-dict (panel-signals panel))])
    (list->integer
      (map (lambda (d)
             (hash-ref dd d))
           (panel-output panel)))))

(struct panel (signals output) #:transparent)
(provide
  (contract-out
    (struct panel ([signals (listof string?)]
                   [output (listof string?)]))))

;-----------------------------------------------------------------------
; Parts one and two
;-----------------------------------------------------------------------

(define (part-1 panels)
  (define (count-1478 output)
    (length
      (filter (lambda (x) (member x '(2 3 4 7)))
              (map string-length output))))
  (foldl + 0
         (map count-1478
              (map panel-output panels))))
(provide
  (contract-out
    [part-1 (-> (listof panel?) integer?)]))

(define (part-2 panels)
  (foldl + 0
         (map get-panel-output panels)))
(provide
  (contract-out
    [part-2 (-> (listof panel?) integer?)]))

;-----------------------------------------------------------------------
; Parsing
;-----------------------------------------------------------------------

(define (parse-input filename)
  (define (parse/line line)
    (let* ([left-right (string-split line " | ")]
           [left (string-split (first left-right) " ")]
           [right (string-split (second left-right) " ")])
      (panel (map alphabetize left)
             (map alphabetize right))))
  (define (iter)
    (let ([line (read-line)])
      (if (eof-object? line)
        '()
        (cons
          (parse/line line)
          (iter)))))
  (with-input-from-file filename iter))
(provide
  (contract-out
    [parse-input (-> string? (listof panel?))]))

;-----------------------------------------------------------------------
; Main
;-----------------------------------------------------------------------

(module+ main
  (displayln "AOC 2021 Day 8")
  (define input (parse-input "input"))
  (displayln
    (format "Part one: ~s" (part-1 input)))
  (displayln
    (format "Part two: ~s" (part-2 input))))
