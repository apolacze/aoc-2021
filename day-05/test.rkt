#lang racket/base

(require "main.rkt")

(module+ test
  (require rackunit)

  (define v1 (vent (posn 0 0) (posn 0 1)))
  (define v2 (vent (posn 0 0) (posn 1 0)))
  (define v3 (vent (posn 0 0) (posn 1 1)))
    
  (check-equal?
    (vent-horizontal? v1)
    #false)
  (check-equal?
    (vent-vertical? v1)
    #true)
  (check-equal?
    (vent-horizontal? v2)
    #true)
  (check-equal?
    (vent-vertical? v2)
    #false)
  (check-equal?
    (vent-vertical? v3)
    #false)
  (check-equal?
    (vent-horizontal? v3)
    #false)
  (check-equal?
    (vent-diagonal? v3)
    #true)


  (check-equal?
    (vent-points/hv v1)
    (list (posn 0 0) (posn 0 1)))
  (check-equal?
    (vent-points/hv v2)
    (list (posn 0 0) (posn 1 0)))
  (check-exn
    exn:fail?
    (lambda ()
      (vent-points/hv v3)))
  (check-equal?
    (vent-points/hv
      (vent (posn 9 4) (posn 8 4)))
    (list (posn 8 4)
          (posn 9 4)))
  (check-equal?
    (vent-points/diagonal v3)
    (list (posn 0 0)
          (posn 1 1)))
  (check-equal?
    (vent-points
      (vent (posn 7 9) (posn 9 7)))
    (list (posn 7 9)
          (posn 8 8)
          (posn 9 7)))

  (check-equal?
    (add-point (make-immutable-hash) (posn 0 0))
    (make-immutable-hash (list (cons (posn 0 0) 1))))

  (check-equal?
    (add-vent (make-immutable-hash) v1)
    (make-immutable-hash (list (cons (posn 0 0) 1)
                               (cons (posn 0 1) 1))))
  (check-equal?
    (add-vent 
      (add-vent (make-immutable-hash) v1)
      v2)
    (make-immutable-hash (list (cons (posn 0 0) 2)
                               (cons (posn 0 1) 1)
                               (cons (posn 1 0) 1))))

  (define input-test
    (parse-input "input-test"))

  (check-equal?
    (part-1 input-test)
    5)

  (check-equal?
    (part-2 input-test)
    12)

  (displayln "End of tests."))
