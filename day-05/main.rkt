#lang racket

;-----------------------------------------------------------------------
; Util
;-----------------------------------------------------------------------

(define (flip f)
  (lambda (x y)
    (f y x)))

;-----------------------------------------------------------------------
; Data structures
;-----------------------------------------------------------------------

(struct posn (x y) #:transparent)
(provide
  (contract-out
    [struct posn ((x integer?) (y integer?))]))

(struct vent (start end) #:transparent)
(provide
  (contract-out
    [struct vent ((start posn?) (end posn?))]))

(define (vent-coords v)
  (let ([x1 (posn-x (vent-start v))]
        [y1 (posn-y (vent-start v))]
        [x2 (posn-x (vent-end v))]
        [y2 (posn-y (vent-end v))])
    (values x1 y1 x2 y2)))

(define (vent-horizontal? v)
  (= (posn-y (vent-start v))
     (posn-y (vent-end v))))
(define (vent-vertical? v)
  (= (posn-x (vent-start v))
     (posn-x (vent-end v))))
(define (vent-diagonal? v)
  (let-values ([(x1 y1 x2 y2) (vent-coords v)])
    (= (abs (- x1 x2))
       (abs (- y1 y2)))))
(provide
  (contract-out
    [vent-horizontal? (-> vent? boolean?)]
    [vent-vertical? (-> vent? boolean?)]
    [vent-diagonal? (-> vent? boolean?)]))

;-----------------------------------------------------------------------
; Vent functions
;-----------------------------------------------------------------------

(define (vent-points/hv v)
  (cond
    [(vent-vertical? v)
     (let* ([start-y (posn-y (vent-start v))]
            [end-y (posn-y (vent-end v))]
            [ys (list start-y end-y)])
       (map (lambda (y) (posn (posn-x (vent-start v)) y))
            (range (apply min ys) (+ (apply max ys) 1))))]
    [(vent-horizontal? v)
     (let* ([start-x (posn-x (vent-start v))]
            [end-x (posn-x (vent-end v))]
            [xs (list start-x end-x)])
       (map (lambda (x) (posn x (posn-y (vent-start v))))
            (range (apply min xs) (+ (apply max xs) 1))))]
    [else
      (error "Vent is neither horizontal nor vertical.")]))
(define (vent-points/diagonal v)
  (let-values ([(x1 y1 x2 y2) (vent-coords v)])
    (let ([sgn-x (if (> x2 x1) 1 -1)]
          [sgn-y (if (> y2 y1) 1 -1)])
      (for/list
        ([i (range x1 (+ sgn-x x2) sgn-x)]
         [j (range y1 (+ sgn-y y2) sgn-y)])
        (posn i j)))))
(define (vent-points v)
  (cond
    [(or (vent-horizontal? v) (vent-vertical? v))
     (vent-points/hv v)]
    [(vent-diagonal? v)
     (vent-points/diagonal v)]
    [else
      (displayln v)
      (error "Only either horizontal, vertical or diagonal vents supported.")]))
(provide
  (contract-out
    [vent-points/hv (-> vent? (listof posn?))]
    [vent-points/diagonal (-> vent? (listof posn?))]
    [vent-points (-> vent? (listof posn?))]))

(define (add-point h p)
  (let ([val (hash-ref h p 0)])
    (hash-set h p (+ val 1))))
(provide
  (contract-out
    [add-point (-> hash? posn? hash?)]))

(define (add-vent h v)
  (let ([ps (vent-points v)])
    (foldl (flip add-point) h ps)))
(provide
  (contract-out
    [add-vent (-> hash? vent? hash?)]))

;-----------------------------------------------------------------------
; Parts one and two
;-----------------------------------------------------------------------

(define (part-1 vents)
  (let* ([vents/hv (filter (lambda (v) (or (vent-horizontal? v)
                                           (vent-vertical? v)))
                           vents)]
         [vent-map (foldl (flip add-vent) (make-immutable-hash) vents/hv)])
    (length
      (filter (lambda (x) (> x 1))
              (hash-values vent-map)))))
(provide
  (contract-out
    [part-1 (-> (listof vent?) integer?)]))

(define (part-2 vents)
  (let ([vent-map (foldl (flip add-vent) (make-immutable-hash) vents)])
    (length
      (filter (lambda (x) (> x 1))
              (hash-values vent-map)))))
(provide
  (contract-out
    [part-2 (-> (listof vent?) integer?)]))

;-----------------------------------------------------------------------
; Parsing
;-----------------------------------------------------------------------

(define (parse-input filename)
  (define (parse/line line)
    (let ([lr (string-split line " -> ")])
      (apply
        vent
        (map parse/posn lr))))
  (define (parse/posn s)
    (let ([xy (string-split s ",")])
      (apply
        posn
        (map string->number xy))))
  (define (iter)
    (let ([line (read-line)])
      (if (eof-object? line)
        '()
        (cons
          (parse/line line)
          (iter)))))
  (with-input-from-file filename iter))
(provide
  (contract-out
    [parse-input (-> string? (listof vent?))]))

;-----------------------------------------------------------------------
; Main
;-----------------------------------------------------------------------

(module+ main
  (displayln "AOC 2021 Day 5")
  (define input (parse-input "input"))
  (displayln
    (format "Part one: ~s" (part-1 input)))
  (displayln
    (format "Part two: ~s" (part-2 input))))
