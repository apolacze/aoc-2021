# Advent of Code 2021

This repository contains my solutions to [Advent of Code
2021](https://adventofcode.com/2021).

All solutions are written in [Racket](https://racket-lang.org/).
