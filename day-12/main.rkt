#lang racket

;-----------------------------------------------------------------------
; Data structures and contracts
;-----------------------------------------------------------------------

(struct cave (type label) #:transparent)
(provide
  (contract-out
    [struct cave ((type symbol?)
                  (label string?))]))

(define cave-system/c
  (hash/c cave? (set/c cave?)))

(define visited/c
  (hash/c cave? integer?))

(struct path (path small-visited) #:transparent)
(provide
  (contract-out
    [struct path ((path (listof cave?))
                  (small-visited visited/c))]))

;-----------------------------------------------------------------------
; Path manipulation and exploration
;-----------------------------------------------------------------------

(define (add-cave-connection cs c1 c2)
  (add-cave-connection/one
    (add-cave-connection/one cs c1 c2)
    c2 c1))
(define (add-cave-connection/one cs c1 c2)
  (let ([st (hash-ref cs c1 (set))])
    (hash-set
      cs c1
      (set-add st c2))))
(provide
  (contract-out
    [add-cave-connection
      (-> cave-system/c cave? cave? cave-system/c)]
    [add-cave-connection/one
      (-> cave-system/c cave? cave? cave-system/c)]))

(define (get-paths cs part)
  (extend-path
    (path
      (list (cave 'start "start"))
      (make-immutable-hash))
    cs
    part))
(provide
  (contract-out
    [get-paths (-> cave-system/c symbol? (listof path?))]))

(define (add-cave-to-path a-cave a-path)
  (let ([new-path (cons a-cave (path-path a-path))]
        [visited (path-small-visited a-path)])
    (path
      new-path
      (if
        (eq? (cave-type a-cave) 'small)
          (hash-update visited
                       a-cave
                       add1
                       0)
          visited))))
(provide
  (contract-out
    [add-cave-to-path
      (-> cave? path? path?)]))

(define (extend-path/one p cs)
  (let ([current (first (path-path p))])
    (let ([connections
            (set->list (hash-ref cs current))])
      (map
        (lambda (connection) (add-cave-to-path connection p))
        connections))))
(provide
  (contract-out
    [extend-path/one
      (-> path? cave-system/c (listof path?))]))

(define (extend-path p cs part)
  (let ([current (first (path-path p))])
    (cond
      [(equal? (cave-type current) 'end)
       (list p)]
      [else
        (apply
          append
          (map
            (lambda (p) (extend-path p cs part))
            (drop-illegal-paths
              (extend-path/one p cs)
              part)))])))
(provide
  (contract-out
    [extend-path
      (-> path? cave-system/c symbol? (listof path?))]))

(define (drop-illegal-paths lop part)
  (let ([no-small-legal?/part
          (cond
            [(eq? part 'one) no-small-legal?/1]
            [(eq? part 'two) no-small-legal?/2])])
  (filter
    (lambda (p)
      (no-small-legal?/part
        (first (path-path p))
        (path-small-visited p)))
    (filter start-legal? lop))))
(provide
  (contract-out
    [drop-illegal-paths
      (-> (listof path?) symbol? (listof path?))]))

(define (start-legal? a-path)
  (not
    (equal? (cave-type (first (path-path a-path)))
            'start)))
(provide
  (contract-out
    [start-legal?
      (-> path? boolean?)]))

(define (no-small-legal?/1 c visited)
  (if (eq? (cave-type c) 'small)
    (if (> (hash-ref visited c 0) 1)
      #f
      #t)
    #t))
(define (no-small-legal?/2 c visited)
  (if (eq? (cave-type c) 'small)
    (let ([n-current (hash-ref visited c)])
      (cond
        [(> n-current 2) #f]
        [(= n-current 2)
         (<=
           (count
             (lambda (v) (= v 2))
             (hash-values visited))
           1)]
        [else #t]))
    #t))
(provide
  (contract-out
    [no-small-legal?/1
     (-> cave? visited/c boolean?)]
    [no-small-legal?/2
     (-> cave? visited/c boolean?)]))

;-----------------------------------------------------------------------
; Parts one and two
;-----------------------------------------------------------------------

(define (part-1 cs)
  (length
    (get-paths cs 'one)))
(provide
  (contract-out
    [part-1 (-> cave-system/c integer?)]))

(define (part-2 cs)
  (length
    (get-paths cs 'two)))
(provide
  (contract-out
    [part-2 (-> cave-system/c integer?)]))

;-----------------------------------------------------------------------
; Parse input
;-----------------------------------------------------------------------

(define (parse-input filename)
  (define (parse/cave-type s)
    (cond
      [(string=? s "start") 'start]
      [(string=? s "end") 'end]
      [(andmap char-upper-case? (string->list s))
       'big]
      [(andmap char-lower-case? (string->list s))
       'small]
      [else
        (error "Error: parse/cave-type:" s)]))
  (define (parse/cave s)
    (cave (parse/cave-type s) s))
  (define (parse/line l cs)
    (let ([caves
            (map parse/cave (string-split l "-"))])
      (match caves
        [(list c1 c2)
         (add-cave-connection
           cs c1 c2)])))
  (for/fold
    ([cs (make-immutable-hash)])
    ([l (file->lines filename)])
    (parse/line l cs)))
(provide
  (contract-out
    [parse-input (-> string? cave-system/c)]))

;-----------------------------------------------------------------------
; Main
;-----------------------------------------------------------------------

(module+ main
  (displayln "AOC 2021 Day 12")
  (define input (parse-input "input"))
  (displayln
    (format "Part one: ~s" (part-1 input)))
  (displayln
    (format "Part two: ~s" (part-2 input))))
